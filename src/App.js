import './App.css';
import React, { useCallback } from 'react';
import ReactFlow, {
  MiniMap,
  Controls,
  Background,
  useNodesState,
  useEdgesState,
  addEdge,
} from 'reactflow';
import {
  Grid,
  getNativeSelectUtilityClasses,
  Modal,
  Box,
  Typography
} from "@mui/material";

import 'reactflow/dist/style.css';


const initialNodes = [
  { id: '1', position: { x: 100, y: 25 }, data: { label: 'Solicitar contratacao de candidato previamente aprovado' } },
  { id: '2', position: { x: 200, y: 120 }, data: { label: 'confirmar recebimento da solicitacao' } },
  { id: '3', position: { x: 300, y: 120 }, data: { label: 'Entrar em contato com o candidato solicitando informacoes nescessarias' } },
  { id: '4', position: { x: 400, y: 200 }, data: { label: 'Encaminhar documentacao solicitada' } },
  { id: '5', position: { x: 500, y: 120 }, data: { label: 'marcar exame admisional' } },
  { id: '6', position: { x: 600, y: 200 }, data: { label: 'Realizar exame admissional e encaminhar atestado' } },
  { id: '7', position: { x: 700, y: 120 }, data: { label: 'encaminhar todas as documentacoes para consultor RH' } },
  { id: '8', position: { x: 800, y: 300 }, data: { label: 'realizar os tramites de registro no e-social e plano de saude' } },
  { id: '9', position: { x: 900, y: 120 }, data: { label: 'criar acesso de colaborador' } },
  { id: '10', position: { x: 1000, y: 120 }, data: { label: 'encaminhar acessos ao colaborador' } },
  { id: '11', position: { x: 1400, y: 200 }, data: { label: 'iniciar as atividades laborais' } },
];
const initialEdges = [
  { id: 'e1-2', source: '1', target: '2' },
  { id: 'e2-3', source: '2', target: '3' },
  { id: 'e3-4', source: '3', target: '4' },
  { id: 'e4-5', source: '4', target: '5' },
  { id: 'e5-6', source: '5', target: '6' },
  { id: 'e6-7', source: '6', target: '7' },
  { id: 'e7-8', source: '7', target: '8' },
  { id: 'e8-9', source: '8', target: '9' },
  { id: 'e9-10', source: '9', target: '10' },
  { id: 'e10-11', source: '10', target: '11' },


];

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};


function App() {
  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onConnect = useCallback(
    (params) => setEdges((eds) => addEdge(params, eds)),
    [setEdges],
  );

  const teste = (event) => {
    event.preventDefault();

    handleOpen();

    console.log(event)

    console.log(event.currentTarget.innerText)

    console.log(event.currentTarget.attributes[1])



  }
  return (
    <Grid >
      <div style={{ width: 1600, height: 500, position: 'relative' }}>
        <img
          src='teste.jpeg'
          style={{ position: 'absolute' }}
        />
        <ReactFlow
          nodes={nodes}
          edges={edges}
          onNodeClick={(event) => teste(event)}
          preventScrolling={false}
          nodesConnectable={false}
          panOnDrag={false}
          zoomOnPinch={false}
          zoomOnDoubleClick={false}
        >
        </ReactFlow>



      </div>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Solicitar de passagens
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            O gestor formaliza um pedido para a contratação de um candidato que já passou
             pelo processo de seleção e foi aprovado. Essa formalização acontece por e-mail,
              que é encaminhado ao RH. É solicitado que a data de início seja no dia 01 ou 15 do mês, por conta do tempo necessário para que possam se seguir os trâmites de modo que não ocorra imprevistos.
          </Typography>
        </Box>
      </Modal>
    </Grid>
  );
}

export default App;
